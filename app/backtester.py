import pandas as pd
import matplotlib.pyplot as plt

from datasets import DATASET_PATH
from app.retrieve_data import STOCKS_CODE


def main():
    ccl_mass = pd.read_csv(f'{DATASET_PATH}/CCL Mass.csv')
    ccl_mass.index = pd.to_datetime(ccl_mass['End Date'])
    ccl_mass['pct_change'] = ccl_mass['CCL Mass']/ccl_mass['CCL Mass'].shift() - 1
    ccl_mass['signal_long'] = ccl_mass['pct_change'] > 0.005
    ccl_mass['signal_short'] = ccl_mass['pct_change'] < -0.005


# def clean_yahoo_finance_bad_data():
#     for s in STOCKS_CODE:
#         df = pd.read_csv(f'{DATASET_PATH}/{s}.csv')
#         df.dropna(inplace=True)
#         df.to_csv(f'{DATASET_PATH}/{s}_modified.csv')


def merge_combined_csv():
    pass


if __name__ == '__main__':
    # clean_yahoo_finance_bad_data()
    main()
