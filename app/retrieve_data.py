import yfinance as yf

from datasets import DATASET_PATH


STOCKS_CODE = ['0005.HK', '0011.HK', '0388.HK', '0939.HK', '1299.HK', '1398.HK',
               '2318.HK', '2388.HK', '2628.HK', '3328.HK', '3988.HK',
               '0012.HK', '0016.HK', '0017.HK', '0101.HK', '0688.HK',
               '0823.HK', '1109.HK', '1113.HK', '1997.HK', '2007.HK', '^HSI']


def main():
    for s in STOCKS_CODE:
        stock = yf.Ticker(s)
        stock.history(period='max').to_csv(f'{DATASET_PATH}/{s}.csv')
        print(f'{s} is saved.')


if __name__ == '__main__':
    main()
